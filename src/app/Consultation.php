<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Consultation
 * @property WkuIdentity wkuIdentity
 * @package App
 */
class Consultation extends Model
{

    CONST ASANA_PROJECT_GID = '1156264213742203';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public static function categories()
    {
        return Category::query()
            ->select(['categories.*'])
            ->join('consultations as c', function($join){
                $join->on('c.category_id', '=', 'categories.id');
            })
            ->groupBy('categories.id')
            ->orderBy('name', 'asc');
    }

    public function citlStaff()
    {
        return $this->belongsTo(CitlStaff::class);
    }

    public static function colleges()
    {
        return College::query()
            ->select('colleges.*')
            ->join('wku_identities as wi', function ($join) {
                $join->on('wi.college_id', '=', 'colleges.id');
                $join->join('consultations as c', function ($join) {
                    $join->on('c.wku_identity_id', '=', 'wi.id');
                });
            })
            ->groupBy('colleges.id')
            ->orderBy('colleges.name', 'asc');
    }

    public function faculty()
    {
        if ( $this->wkuIdentity()->exists() ) {
            return $this->wkuIdentity->faculty();
        } else {
            return $this->wkuIdentity();
        }
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }

    /**
     * @return Builder
     */
    public static function semesters()
    {
        return  Semester::query()
            ->select([
                'semesters.*',
                DB::raw('count(c.id) as consultation_count'),
            ])
            ->join('consultations as c', function($join) {
                $join->on('c.semester_id', '=', 'semesters.id');
            })
            ->leftJoin('wku_identities as wi', function ($join) {
                $join->on('wi.id', '=', 'c.wku_identity_id');
            })
            ->groupBy('semesters.id');
    }

    public function wkuIdentity()
    {
        return $this->belongsTo(WkuIdentity::class);
    }

}
